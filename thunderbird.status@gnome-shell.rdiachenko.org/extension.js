// Creates a system status notification icon for Thunderbird
const StatusIconDispatcher = imports.ui.statusIconDispatcher;

function enable() {
    StatusIconDispatcher.STANDARD_TRAY_ICON_IMPLEMENTATIONS['thunderbird'] = 'thunderbird';
}

function disable() {
    StatusIconDispatcher.STANDARD_TRAY_ICON_IMPLEMENTATIONS['thunderbird'] = '';
}

// gnome-shell extension entry point
function init() {
}
